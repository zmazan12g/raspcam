# RaspCam
### Video Notes
##### Monday, 2023-01-02

---

### Location
Dork's Basement

---

### Considerations
- Routing the the ribbon cable through the enclosure allowing for unobstructed movement
- Mounting camera module into enclosure with proper fitment
- Joining servomotor and enclosure with proper fitment
- Fitment of final assembly into bottom stand housing *(might not potentially be wide enough)*

---

### Resources

#### Guides
- [Build Article](https://smartbuilds.io/smart-cctv-camera-flask-video-streaming-opencv-raspberry-pi)
- [Face Recognition Installation Article](https://smartbuilds.io/installing-face-recognition-library-on-raspberry-pi-4/)
- [Build Video](https://www.youtube.com/watch?v=6nY-V_WG7oI)

#### Software
- [Raspberry Pi OS Operating System Images](https://www.raspberrypi.com/software/operating-systems/)

---

### Parts Inventory
- [**Raspberry Pi** 4](https://assets.raspberrypi.com/static/raspberry-pi-4-labelled-f5e5dcdf6a34223235f83261fa42d1e8.png)
- [**Raspberry Pi** 4 PSU](https://m.media-amazon.com/images/I/61pj7sQU3qL._AC_SL1280_.jpg)
- [**Raspberry Pi** HQ Camera Module](https://m.media-amazon.com/images/I/71RG72G-GqL._AC_SL1500_.jpg)
- [**Raspberry Pi** HQ Arducam Lens](https://m.media-amazon.com/images/I/61fLFlLr05L._AC_SL1500_.jpg)
- [Set of 5 Micro Servo Motors](https://m.media-amazon.com/images/I/61j7+y078qL._AC_SL1000_.jpg)
- [Jumper Wire Assorted Kit](https://m.media-amazon.com/images/I/71wNuDUZGEL._SL1300_.jpg)
- [Surveillance Camera Enclosure](https://m.media-amazon.com/images/I/51M+up05JtL._AC_SL1500_.jpg)

### Tools Inventory
- Computer
- SD Card
- iFixit Kit
- Drill
- Soldering Iron

### Consumables Inventory
- Heatshrink

---

### Steps

#### Preliminary
- [ ] Plug camera into **Pi**
- [ ] Raspberry OS installation to SD Card
- [ ] Test camera by taking a picture

#### Build
- [ ] Drill hole for passing **Pi** ribbon cable through
- [ ] Screw in camera module into enclosure ball
- [ ] Reassemble both pieces of the enclosure
- [ ] Adapt cable connectors of servomotor to **Pi** sockets
- [ ] Drill hole thorugh enclosure ball to mount to servomotor
- [ ] Join the servomotor and enclosure
- [ ] Connect jumper cables between servomotor and **Pi** sockets
    - [ ] Connect to Pin 4 (+5V)
    - [ ] Connect to Pin 6 (GND)
    - [ ] Connect to Pin 11 (GPIO)
- [ ] Connect camera module ribbon cable to **Pi**
- [ ] Reinsert SD Card with OS into **Pi** after completing the assembly process
- [ ] Plug in unit to power using the PSU preferrably

#### Software
- [ ] Setup software for remote desktop access to unit
- [ ] Complete the **Pi** OS setup
    - [ ] Enable camera port for **Pi** in OS if is not already
    - [ ] Run the following commands for setup:
        - [ ] `sudo rasp-config`
        - [ ] `raspistill -o Desktop/image.jpg`
    - [ ] Check necessary dependencies are installed, *install if needed*
        - [ ] `sudo apt-get update`
        - [ ] `sudo apt-get upgrade`
        - [ ] `sudo apt-get install build-essential`
        - [ ] `sudo apt-get install cmake`
        - [ ] `sudo apt-get install gfortran`
        - [ ] `sudo apt-get install git`
        - [ ] `sudo apt-get install wget`
        - [ ] `sudo apt-get install curl`
        - [ ] `sudo apt-get install graphicsmagick`
        - [ ] `sudo apt-get install libgraphicsmagick1-dev`
        - [ ] `sudo apt-get install libatlas-base-dev`
        - [ ] `sudo apt-get install libavcodec-dev`
        - [ ] `sudo apt-get install libavformat-dev`
        - [ ] `sudo apt-get install libboost-all-dev`
        - [ ] `sudo apt-get install libgtk2.0-dev`
        - [ ] `sudo apt-get install libjpeg-dev`
        - [ ] `sudo apt-get install liblapack-dev`
        - [ ] `sudo apt-get install libswscale-dev`
        - [ ] `sudo apt-get install pkg-config`
        - [ ] `sudo apt-get install python3-dev`
        - [ ] `sudo apt-get install python3-numpy`
        - [ ] `sudo apt-get install python3-pip`
        - [ ] `sudo apt-get install zip`
        - [ ] `sudo apt-get clean`
        - [ ] `sudo apt-get install python3-picamera`
        - [ ] `sudo pip3 install --upgrade picamera[array]`
    - [ ] Install libraries
        - [ ] **dlib library**
            - [ ] Increase swapfile size to improve performance for process *(ex. 100 to 1024)*
            - [ ] Install **dlib library**
            - [ ] Decrease swapfile size to original size *(ex. 1024 to 100)*
        - [ ] ***Additional libraries***
            - [ ] `pip3 install numpy`
            - [ ] `pip3 install scikit-image`
            - [ ] `sudo apt-get install python3-scipy`
            - [ ] `sudo apt-get install libatlas-base-dev`
            - [ ] `sudo apt-get install libjasper-dev`
            - [ ] `sudo apt-get install libqtgui4`
            - [ ] `sudo apt-get install python3-pyqt5`
            - [ ] `sudo apt install libqt4-test`
            - [ ] `pip3 install opencv-python==3.4.6.27`
            - [ ] `pip3 install face_recognition `
        - [ ] **face recognition library**
            - [ ] Install **face recognition library**
    - [ ] Clone Smart CCTV git repository
        - [ ] `cd ~`
        - [ ] `git clone --single-branch https://github.com/EbenKouao/SmartCCTV-Camera`
        - [ ] `SmartCCTV-Camera/main.py` will start streaming for the livestream
        - [ ] Images of faces can be placed in `SmartCCTV-Camera/profiles/` to be detected by the API.
    - [ ] Test the unit by accessing the live stream with face recognition
        - [ ] Run main.py contained within the repository
        - [ ] Visit the IP address of the **Pi** in a browser connected to the same network
        - [ ] Interact with the camera to test it
    - [ ] Add profiles to face recogntion
        - [ ] 
