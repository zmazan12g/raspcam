# Raspcam Project Supplies

## Tools
- [ ] SD Card
- [ ] iFixit Kit
- [ ] Drill *(Any tool to make the hole will work fine)*
- [ ] Soldering Station
- [ ] Heatshrink

## Parts
- [ ] [**Raspberry Pi** 4 PSU](https://m.media-amazon.com/images/I/61pj7sQU3qL._AC_SL1280_.jpg)
- [ ] [**Raspberry Pi** HQ Camera Module](https://m.media-amazon.com/images/I/71RG72G-GqL._AC_SL1500_.jpg)
- [ ] [**Raspberry Pi** HQ Arducam Lens](https://m.media-amazon.com/images/I/61fLFlLr05L._AC_SL1500_.jpg)
- [ ] [Set of 5 Micro Servo Motors](https://m.media-amazon.com/images/I/61j7+y078qL._AC_SL1000_.jpg)
- [ ] [Jumper Wire Assorted Kit](https://m.media-amazon.com/images/I/71wNuDUZGEL._SL1300_.jpg)
- [ ] [Surveillance Camera Enclosure](https://m.media-amazon.com/images/I/51M+up05JtL._AC_SL1500_.jpg)
